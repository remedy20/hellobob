import app as app
import unittest
import requests


class UnitTest(unittest.TestCase):
    def setUp(self):
        self.app = app.app.test_client()
    
    def test_add(self):
        response = self.app.get('/add?a=10&b=10')
        self.assertEqual(int(response.data),20)

    def test_sub(self):
        response = self.app.get('/sub?a=10&b=10')
        self.assertEqual(int(response.data),0)

if __name__ == "__main__":
    unittest.main()
