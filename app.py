from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def index():
   return 'Hello Bob from workspace'

@app.route('/add',methods=['GET'])
def add():
	if request.method == "GET":
		a = int(request.args.get('a'))
		b = int(request.args.get('b'))
		response = str(a+b)
		return response
	else:
		return

@app.route('/sub',methods=['GET'])
def sub():
	if request.method == "GET":
		a = int(request.args.get('a'))
		b = int(request.args.get('b'))
		response = str(a-b)
		return response
	else:
		return
if __name__ == "__main__":
   app.run(host='0.0.0.0',port=8014)
